---
layout: handbook-page-toc
title: "Internal community contributions swag"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Let's Award Contributors Together

Each team member is empowered to send GitLab swag to anyone in the wider GitLab community who deserves it.
We'll be using Printfection's giveaway links to make this happen.

## Eligibility
 
Anyone who you feel deserves an extra special something for their contribution.
This can include, (but is not limited to): blog post authors, a user providing a tip or trick, or someone who made a video tutorial about GitLab. Use your best judgement.

Please update this section with other examples of contributions as we want to include everybody!
* A Power User in our [community forum](https://forum.gitlab.com/)
* Time spent with our Product team providing insight and feedback on features

## Giveaway links

The list of the giveaways is found in this [Google Sheets file](https://docs.google.com/spreadsheets/d/1kzK25SE9jPXx50Lw0W8jtm9u77Zr4w13FAg1gyD6Egk/edit?usp=sharing) editable only by GitLab team members.

## Sending swag

Once you've decided to award the community contributor, please open the [sheet file](https://docs.google.com/spreadsheets/d/1kzK25SE9jPXx50Lw0W8jtm9u77Zr4w13FAg1gyD6Egk/edit?usp=sharing) and follow these steps:

* Pick the first available (not used or sent) link in the row
* Fill the 'Sent to Name' field with the contributor's full name
* [Optional] Fill the 'Sent to Email' field with the contributor email address
* Fill the 'Requester' field with your name
* Reach out personally to the GitLab contributor with the giveaway URL, and let them know they simply need to pick an item, and input their shipping details 

Note: This data is required for Community Advocates to monitor the placed orders within Printfection.

## Include Everyone

If you think that someone deserves a swag prize, feel free to award them with swag via a Printfection link.

## Refresh Giveaway Codes

1. Log into Printfection
2. Navigate to Campaigns -> Giveaways -> Community Contributions
3. Under the section titled 'Giveaway Links' you can see how many links have been created, sent, and redeemed for this campaign
4. Click the 'Get New Link' button to generate a new giveaway link
5. Copy and paste the new link into the [Google Sheets file](https://docs.google.com/spreadsheets/d/1kzK25SE9jPXx50Lw0W8jtm9u77Zr4w13FAg1gyD6Egk/edit?usp=sharing) editable only by GitLab team members
6. When this Google Sheet becomes too long, consider archiving the sheet or creating a new tab with current giveaway links