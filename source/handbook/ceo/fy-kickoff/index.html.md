---
layout: handbook-page-toc
title: "Fiscal Year Kickoff"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Fiscal Year Kickoff

In the first week of every Fiscal Year (first week of February), the E-Group hosts a Fiscal Year Kickoff.
The call is 80 minutes long and takes place over Zoom.
The doc is circulated early so that all time zones can submit questions.

The goal of the Kickoff is to discuss the strategy and plans for the next fiscal year.
Each member of the [e-group](/handbook/leadership/#e-group) has 5 minutes to discuss their part of the business.

The call concludes with an AMA with all executives.
Team members can ask questions of any particular executive or the execs may choose amongst themselves who responds.

The [Chief of Staff](/job-families/chief-executive-officer/chief-of-staff/) facilitates the call, noting especially to keep people on time. 
The meeting is scheduled by the [EBA to the CEO](/job-families/people-ops/executive-business-administrator/).
The call is recorded and posted to GitLab Unfiltered.
