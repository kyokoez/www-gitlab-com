---
layout: handbook-page-toc
title: "Acquisition Offer"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

This page has been deprecated, please refer to our [acquisitions handbook](/handbook/acquisitions) for information on acquisitions.
