---
layout: job_family_page
title: Director, Total Rewards
---

The Director, Total Rewards leads the compensation and benefits function ensuring our total rewards strategy and practices continue to support our culture and growth. This role is a senior member of the People Group team who is responsible for building out the Total Rewards function and mentoring current team members. The Director, Total Rewards reports to the Chief People Officer.

## Responsibilities

* Review and analyze GitLab’s total rewards (compensation, benefits, perks, recognition) practices globally and in relation to local markets; propose improvements to remain competitive.
* Maintain expertise in industry practices and lead effective, competitive, and fair total rewards programs that ensure market consistency and cost-effectiveness.
* Design, develop, and execute on a comprehensive global total rewards strategy that aligns with our culture and business plans, and allows GitLab to attract, retain, and motivate top talent.
* Create scalable processes, practices, and programs that support our rapid growth while continuing to deliver an incredible employee experience.
* Oversee and support the development, implementation, and maintenance or compensation tools and systems.
* Oversee our benefits programs including insurance, retirement, time off, etc..
* Own the communications strategy and plan for total rewards, ensuring team members understand all that’s offered at GitLab.
* Lead the team to perform effective job evaluations/leveling, conduct market surveys, collect and analyze market data, and maintain compensation survey data; continuously updating our compensation ranges and job structure.
* Develop recommendations for annual budgeting and planning while maintaining internal and external equity in pay plans; manage annual compensation review cycles.
* Develop and manage executive compensation; review, design, develop, and manage variable incentive pay for non-sales.
* Lead stock administration and ensure accurate information is provided to team members about stock grants, vesting schedules, exercise process, and eligibility.
* Manage equity planning and implementation of new schemes as needed; partner with Chief Financial Officer to manage the cap table.
* Work closely with the Chief People Officer to develop material for executive management and the Compensation Committee.
* Accurately interpret, counsel, communicate, and educate People Group members, managers, and executives on pay philosophy, policies, and practices.

## Requirements

* Minimum 8 years of progressive experience in total rewards with in-depth knowledge of core compensation and job structure concepts and standard methodologies.
* Experience designing and managing compensation and benefits programs, ideally at rapidly growing, global companies in a relevant industry.
* Forward thinking, creative, and open-minded with sound technical skills, analytical ability, and seasoned judgment.
* Comfortable and enthusiastic about working in a fast paced, high growth, constantly changing, geographically dispersed, transparent environment.
* Ability to drive consensus and engagement across a wide variety of stakeholders in multiple parts of the business.
* Data-driven leader with a strong ability to analyze and turn data into insights and action plans aligned with company direction.
* Excellent verbal and written communication skills, ability to package and present complex analyses and recommendations clearly.
* Previous experience in both public and startup companies; IPO experience is ideal.
* Experience preparing for and interacting with the Compensation Committee.
* Experience working remotely and with remote team members is preferred.
* Ability to use GitLab

## Performance Indicators

Primary performance indicators for this role:

- [Team member retention](/handbook/people-group/people-operations-metrics/#team-member-retention)
- [Percentage over compensation band](/handbook/people-group/people-operations-metrics/#percent-over-compensation-band)
- [Average location factor](/handbook/people-group/people-operations-metrics/#average-location-factor)
- [New hire location factor](/handbook/hiring/metrics/#new-hire-location-factor)
- [Pay equality](/company/culture/inclusion/#performance-indicators)
- [Spend per team member](/handbook/people-group/people-operations-metrics/#cost-per-team-member)

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team/).

   * Qualified candidates will be invited to schedule a 30 minute screening call with a Recruiter
   * Then, candidates will be invited to schedule a 50 minute interview with the Hiring Manager
   * Next, candidates will be invited to schedule 50 minute interviews with Director, Global People Operations, and the Manager, Compensation & Benefits, and a People Business Partner
   * Finally, candidates will be invited to schedule 50 minute interviews with the Chief Financial Officer as well as another executive.

As always, the interviews and screening call will be conducted via a [video call](/handbook/communication/#video-calls). See more details about our interview process [here](/handbook/hiring/interviewing/).
